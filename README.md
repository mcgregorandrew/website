# README #

This repo contains the files for the 
Research Platform for Smart Facilities Management (RPSMARF) front-end website.

### What is in this repository? ###

* This repo contains HTML and CSS source code and images for the main website.

### How do I get set up? ###

* Summary of set up  
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* See andrewmcgregor@sce.carleton.ca or melendez@sce.carleton.ca