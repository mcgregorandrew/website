'''
    Copyright (C) 2014 Carleton University

    This file is part of RP-SMARF.

    RP-SMARF is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RP-SMARF is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RP-SMARF.  If not, see <http://www.gnu.org/licenses/>.
   
    Created on Sep 8, 2014

    @author: rpsmarf
'''
import glob
import os
import fnmatch
import datetime

debFiles = []
for root, dirnames, filenames in os.walk('rpSmarf_website'):
    for filename in filenames:
        debFiles.append(os.path.join(root, filename))
installFiles = glob.glob("install/*")
debFiles += installFiles

SMARF_USER = "unset"
SMARF_HOST = "unset"
if ("SMARF_HOST" in os.environ):
    SMARF_HOST = os.environ["SMARF_HOST"]
if ("SMARF_GITDIR" in os.environ):
    SMARF_GITDIR = os.environ["SMARF_GITDIR"]
if ("SMARF_USER" in os.environ):
    SMARF_USER = os.environ["SMARF_USER"]
t = datetime.datetime.today()
SMARF_VERSION = t.strftime("%Y%m%d%H%M%S") + "-" + SMARF_USER
    
SSH_CMD = ("ssh  -o UserKnownHostsFile=/dev/null " + 
"-o StrictHostKeyChecking=no " + 
"-i ../tools/keys/smarf_dair_keys.pem ubuntu@" + SMARF_HOST)

DOIT_CONFIG = {'default_tasks': ['show']}


def task_show():
    """show all available tasks"""
    return {'actions': ['doit list'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_keysetup():
    """show all available tasks"""
    return {'actions': ['rm -f build_output/smarf_dair_keys.pem',
                        'cp ../python_repo/tools/keys/smarf_dair_keys.pem build_output/smarf_dair_keys.pem',
                        'chmod 400 build_output/smarf_dair_keys.pem'],
            'file_dep': ["../python_repo/tools/keys/smarf_dair_keys.pem"],
            'targets': ["build_output/smarf_dair_keys.pem"],
            'verbosity': 2,
            }


def task_vlist():
    """show all available versions of smarf-website on the software server"""
    return {'actions': ['sudo apt-get update > /dev/null',
                        'apt-cache show smarf-website | grep Version'],
            'file_dep': [],
            'targets': [],
            'verbosity': 2,
            }


def task_deb():
    """create .deb file for the SMARF website"""
    return {'actions': [
            'rm -rf build_output/smarf-website',
            'mkdir -p build_output/smarf-website',
            'mkdir build_output/smarf-website/DEBIAN',
            'sed -e s/@VERSION/' + SMARF_VERSION + 
'/ install/control > build_output/smarf-website/DEBIAN/control',
            'mkdir -p build_output/smarf-website/var/www',
            'cp -r rpSmarf_website/* build_output/smarf-website/var/www',
            'cd build_output;dpkg -b smarf-website',
            ],
            'targets': ['build_output/smarf-website.deb'],
            'file_dep': debFiles,
            'clean': True,
            }


def task_install():
    """install SMARF Website DEB package locally"""
    return {'actions': ['sudo dpkg -r smarf-website',
                        'sudo dpkg -i build_output/smarf-website.deb'],
            'file_dep': ['build_output/smarf-website.deb'],
            'verbosity': 2,
            'clean': ["sudo dpkg --purge smarf-website"],
            }

def task_rinstall():
    """install the currently building SRA DEB package remotely"""
    return {'actions': [
            'scp -q -o UserKnownHostsFile=/dev/null '
'-o StrictHostKeyChecking=no '
'-i build_output/smarf_dair_keys.pem build_output/smarf-website.deb ubuntu@' + 
SMARF_HOST + ':/tmp',
            SSH_CMD + ' "sudo dpkg -i /tmp/smarf-website.deb"',
            ],
            'file_dep': ['build_output/smarf-website.deb',
                         'build_output/smarf_dair_keys.pem'],
            'verbosity': 2,
            }

def task_debup():
    """create and upload .deb file for the website"""
    return {'actions': 
            ['scp -q -o UserKnownHostsFile=/dev/null '
             '-o StrictHostKeyChecking=no '
            '-i build_output/smarf_dair_keys.pem '
            'build_output/smarf-website.deb '
            'ubuntu@sw.rpsmarf.ca:/var/www/debs/all/smarf-website-' + 
             SMARF_VERSION + '.deb',
           'ssh  -o UserKnownHostsFile=/dev/null '
            '-o StrictHostKeyChecking=no '
            '-i build_output/smarf_dair_keys.pem ubuntu@sw.rpsmarf.ca '
            '"cd /var/www/debs;dpkg-scanpackages '
            '-m all| gzip -9c > all/Packages.gz"'
             ],
            'file_dep': ['build_output/smarf-website.deb',
                         'build_output/smarf_dair_keys.pem'],
            'targets': [],
            'verbosity': 2,
            }


